/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machudh.oauthserver.outhserver.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Moh Machfudh
 */

@Data
@Entity @Table(name = "net_user" )
public class User {
    
    @Id 
    private String id;
    
    private String username;
    
    private String password;
    
    private boolean enabled;
    
    private String userinsert;
    
    private String userdate;
    
}
