/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machudh.oauthserver.outhserver.dao;

import com.machudh.oauthserver.outhserver.dto.User;
import java.io.Serializable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Moh Machfudh
 */
public interface UserDao extends PagingAndSortingRepository<User, String>{
    
}
