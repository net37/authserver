/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machudh.oauthserver.outhserver.controller;

import com.machudh.oauthserver.outhserver.dao.UserDao;
import com.machudh.oauthserver.outhserver.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Moh Machfudh
 */

@Controller
public class UserController {
    
    @Autowired
    private UserDao userDao;
    
  @GetMapping("/api/user")
  @ResponseBody
  public Page<User> dataUser(Pageable page){
      return userDao.findAll(page);
  }
    
}
