package com.machudh.oauthserver.outhserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@Controller
@EnableDiscoveryClient
public class OuthserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(OuthserverApplication.class, args);
    }

    @GetMapping("/protected")
    public void protectedPage() {

    }

}
