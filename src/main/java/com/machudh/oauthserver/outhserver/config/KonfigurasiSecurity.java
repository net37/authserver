/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machudh.oauthserver.outhserver.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *
 * @author Moh Machfudh
 */
@Configuration
@EnableWebSecurity
public class KonfigurasiSecurity extends WebSecurityConfigurerAdapter{
    
    @Autowired
    private DataSource dataSource;
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(passwordEncoder())
                .usersByUsernameQuery("select username, password, enabled " +
                                      "from net_user " +
                                      "where username = ? ")
                .authoritiesByUsernameQuery("select u.username, p.name " +
                                            "from net_user u " +
                                            "inner join net_user_role ur on u.id = ur.id_user " +
                                            "inner join net_role r on r.id = ur.id_role " +
                                            "inner join net_role_permission rp on rp.id_role = r.id " +
                                            "inner join net_permission p on p.id = rp.id_permission " +
                                            "where u.username = ?") ;
                
//        auth.inMemoryAuthentication()
//                .withUser("Machfudh")
//                .password("123456")
//                .authorities("ROLE_ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       http.formLogin().permitAll()
                .and().logout()
                .and()
                .authorizeRequests().antMatchers("/api/*").permitAll()
                .and().authorizeRequests().anyRequest().authenticated();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Bean
    public UserDetailsService userDetailsServiceBean() {
        return super.userDetailsService(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    
}
